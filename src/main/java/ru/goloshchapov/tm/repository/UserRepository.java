package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {return users;}

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findUserById(final String id) {
        for (final User user: users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findUserByLogin(final String login) {
        for (final User user: users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findUserByEmail(final String email) {
        for (final User user: users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public final boolean isLoginExists(final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public final boolean isEmailExists(final String email) {
        return findUserByEmail(email) != null;
    }

    @Override
    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findUserById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findUserByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}
