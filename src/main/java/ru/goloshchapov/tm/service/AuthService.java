package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.service.IAuthService;
import ru.goloshchapov.tm.api.service.IUserService;
import ru.goloshchapov.tm.exception.auth.AccessDeniedException;
import ru.goloshchapov.tm.exception.empty.EmptyLoginException;
import ru.goloshchapov.tm.exception.empty.EmptyPasswordException;
import ru.goloshchapov.tm.model.User;

import static ru.goloshchapov.tm.util.HashUtil.salt;
import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public final String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public final User getUser() {
        final String userId = getUserId();
        return userService.findUserById(userId);
    }

    @Override
    public final boolean isAuth() { return userId == null; }

    @Override
    public void logout() { userId = null; }

    @Override
    public void login(final String login, final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        final User user = userService.findUserByLogin(login);
        final String hash = salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

    @Override
    public void registry(final String login,
                         final String password,
                         final String email,
                         final String role
    ) {
        userService.create(login, password, email, role);
    }

}
