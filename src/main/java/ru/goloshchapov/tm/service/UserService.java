package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.repository.IUserRepository;
import ru.goloshchapov.tm.api.service.IUserService;
import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.exception.auth.EmailExistsException;
import ru.goloshchapov.tm.exception.auth.LoginExistsException;
import ru.goloshchapov.tm.exception.empty.*;
import ru.goloshchapov.tm.exception.entity.UserByEmailNotFoundException;
import ru.goloshchapov.tm.exception.entity.UserByIdNotFoundException;
import ru.goloshchapov.tm.exception.entity.UserByLoginNotFoundException;
import ru.goloshchapov.tm.exception.incorrect.RoleIncorrectException;
import ru.goloshchapov.tm.model.User;

import java.util.List;

import static ru.goloshchapov.tm.util.HashUtil.salt;
import static ru.goloshchapov.tm.util.ValidationUtil.*;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    private String[] toStringArray(final Object[] objects) {
        final int length = objects.length;
        if (length == 0) return null;
        String[] stringObjects = new String[length];
        for (int i=0; i<length; i++) stringObjects[i] = objects[i].toString();
        return stringObjects;
    }

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public final boolean isLoginExists(final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return userRepository.isLoginExists(login);
    }

    @Override
    public final boolean isEmailExists(final String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        return userRepository.isEmailExists(email);
    }

    @Override
    public User create(final String login, final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new LoginExistsException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyIdException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final Role[] roles = Role.values();
        final String roleChecking = role.toString();
        if (!checkInclude(roleChecking,toStringArray(roles))) throw new RoleIncorrectException(roleChecking);
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email, final String role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (isEmpty(role)) throw new EmptyRoleException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        Role[] roles = Role.values();
        if (!checkInclude(role, toStringArray(roles))) throw new RoleIncorrectException(role);
        final Role roleChecked = Role.valueOf(role);
        final User user = create(login, password, email);
        if (user == null) return null;
        user.setRole(roleChecked);
        return user;
    }

    @Override
    public User findUserById(String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final User user = userRepository.findUserById(id);
        if (user == null) throw new UserByIdNotFoundException(id);
        return user;
    }

    @Override
    public User findUserByLogin(String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        final User user = userRepository.findUserByLogin(login);
        if (user == null) throw new UserByLoginNotFoundException(login);
        return user;
    }

    @Override
    public User findUserByEmail(String email) {
        if (isEmpty(email)) throw new EmptyEmailException();
        final User user = userRepository.findUserByEmail(email);
        if (user == null) throw new UserByEmailNotFoundException(email);
        return user;
    }

    @Override
    public User removeUser(User user) {
        return userRepository.removeUser(user);
    }

    @Override
    public User removeUserById(String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final User user = userRepository.removeUserById(id);
        if (user == null) throw new UserByIdNotFoundException(id);
        return user;
    }

    @Override
    public User removeUserByLogin(String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        final User user = userRepository.removeUserByLogin(login);
        if (user == null) throw new UserByLoginNotFoundException(login);
        return user;
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        final User user = findUserById(userId);
        if (user == null) throw new UserByIdNotFoundException(userId);
        final String hash = salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        if (isEmpty(userId)) throw new EmptyIdException();
        final User user = findUserById(userId);
        if (user == null) throw new UserByIdNotFoundException(userId);
        user.setFirstname(firstName);
        user.setLastname(lastName);
        user.setMiddlename(middleName);
        return user;
    }

}
