package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.service.ITaskService;
import ru.goloshchapov.tm.constant.SortConst;
import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.incorrect.StatusIncorrectException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    private String[] toStringArray(final Object[] objects) {
        final int length = objects.length;
        if (length == 0) return null;
        String[] stringObjects = new String[length];
        for (int i=0; i<length; i++) stringObjects[i] = objects[i].toString();
        return stringObjects;
    }

    private void checkDateByStatus(final Task task) {
        if (task == null) return;
        final Status status = task.getStatus();
        final Date dateStart = task.getDateStart();
        final Date dateFinish = task.getDateFinish();
        final Date dateNow = new Date();
        switch (status) {
            case COMPLETE:
                if (dateStart == null) task.setDateStart(dateNow);
                if (dateFinish == null) task.setDateFinish(dateNow);
                break;
            case IN_PROGRESS:
                if (dateStart == null) task.setDateStart(new Date());
                task.setDateFinish(null);
                break;
            default:
                task.setDateStart(null);
                task.setDateFinish(null);
        }
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAllStarted(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAllStarted(comparator);
    }

    @Override
    public List<Task> findAllCompleted(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAllCompleted(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> sortedBy(final String sortCheck) {
        final Sort[] sortOptions = Sort.values();
        String sortChoice = SortConst.STATUS_DEFAULT;
        if (!isEmpty(sortCheck) && checkInclude(sortCheck,toStringArray(sortOptions))) sortChoice = sortCheck;
        final Sort sortType = Sort.valueOf(sortChoice);
        switch (sortType) {
            case DATE_START: return findAllStarted(sortType.getComparator());
            case DATE_FINISH: return findAllCompleted(sortType.getComparator());
            default: return findAll(sortType.getComparator());
        }
    }

    @Override
    public Task findOneById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task removeOneById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        final int size = taskRepository.size();
        if (checkIndex(index,size)) return taskRepository.findOneByIndex(index);
        else throw new IndexIncorrectException();
    }

    @Override
    public Task findOneByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final int size = taskRepository.size();
        if (checkIndex(index,size)) return taskRepository.removeOneByIndex(index);
        else throw new IndexIncorrectException();
    }

    @Override
    public Task removeOneByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task updateOneById(final String id, final String name, final String description) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (taskRepository.isAbsentById(id)) return null;
        final Task task = findOneById(id);
        task.setId(id);
        task.setName(name);
        task.setDescription(name);
        return task;
    }

    @Override
    public Task updateOneByIndex(final Integer index, final String name, final String description) {
        final int size = taskRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (taskRepository.isAbsentByIndex(index)) return null;
        final Task task = findOneByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = taskRepository.startTaskById(id);
        checkDateByStatus(task);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        final int size = taskRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        final Task task = taskRepository.startTaskByIndex(index);
        checkDateByStatus(task);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = taskRepository.startTaskByName(name);
        checkDateByStatus(task);
        return task;
    }

    @Override
    public Task finishTaskById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = taskRepository.finishTaskById(id);
        checkDateByStatus(task);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        final int size = taskRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        final Task task = taskRepository.finishTaskByIndex(index);
        checkDateByStatus(task);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = taskRepository.finishTaskByName(name);
        checkDateByStatus(task);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final String statusChange) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (taskRepository.isAbsentById(id)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        final Status status = Status.valueOf(statusChange);
        final Task task = findOneById(id);
        task.setStatus(status);
        checkDateByStatus(task);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String name, final String statusChange) {
        if (isEmpty(name)) throw new EmptyNameException();
        if (taskRepository.isAbsentByName(name)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        final Status status = Status.valueOf(statusChange);
        final Task task = findOneByName(name);
        task.setStatus(status);
        checkDateByStatus(task);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final int index, final String statusChange) {
        final int size = taskRepository.size();
        if (!checkIndex(index, size)) throw new IndexIncorrectException();
        if (taskRepository.isAbsentByIndex(index)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        final Status status = Status.valueOf(statusChange);
        final Task task = findOneByIndex(index);
        task.setStatus(status);
        checkDateByStatus(task);
        return task;
    }

}
