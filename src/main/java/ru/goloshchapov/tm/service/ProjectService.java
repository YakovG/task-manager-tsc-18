package ru.goloshchapov.tm.service;

import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.api.service.IProjectService;
import ru.goloshchapov.tm.constant.SortConst;
import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.empty.EmptyIdException;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.exception.incorrect.StatusIncorrectException;
import ru.goloshchapov.tm.exception.system.IndexIncorrectException;
import ru.goloshchapov.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ru.goloshchapov.tm.util.ValidationUtil.*;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    private String[] toStringArray(final Object[] objects) {
        final int length = objects.length;
        if (length == 0) return null;
        String[] stringObjects = new String[length];
        for (int i=0; i<length; i++) stringObjects[i] = objects[i].toString();
        return stringObjects;
    }

    private void checkDateByStatus(final Project project) {
        final Status status = project.getStatus();
        final Date dateStart = project.getDateStart();
        final Date dateFinish = project.getDateFinish();
        final Date dateNow = new Date();
        switch (status) {
            case COMPLETE:
                if (dateStart == null) project.setDateStart(dateNow);
                if (dateFinish == null) project.setDateFinish(dateNow);
                break;
            case IN_PROGRESS:
                if (dateStart == null) project.setDateStart(new Date());
                project.setDateFinish(null);
                break;
            default:
                project.setDateStart(null);
                project.setDateFinish(null);
        }
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAllStarted(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAllStarted(comparator);
    }

    @Override
    public List<Project> findAllCompleted(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAllCompleted(comparator);
    }

    @Override
    public Project add(final String name, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> sortedBy(final String sortCheck) {
        final Sort[] sortOptions = Sort.values();
        String sortChoice = SortConst.STATUS_DEFAULT;
        if (!isEmpty(sortCheck) && checkInclude(sortCheck,toStringArray(sortOptions))) sortChoice = sortCheck;
        final Sort sortType = Sort.valueOf(sortChoice);
        switch (sortType) {
            case DATE_START: return findAllStarted(sortType.getComparator());
            case DATE_FINISH: return findAllCompleted(sortType.getComparator());
            default: return findAll(sortType.getComparator());
        }
    }

    @Override
    public Project findOneById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project removeOneById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        final int size = findAll().size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final int size = findAll().size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project updateOneById(final String id, final String name, final String description) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (projectRepository.isAbsentById(id)) return null;
        final Project project = findOneById(id);
        project.setId(id);
        project.setName(name);
        project.setDescription(name);
        return project;
    }

    @Override
    public Project updateOneByIndex(final Integer index, final String name, final String description) {
        final int size = findAll().size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        if (projectRepository.isAbsentByIndex(index)) return null;
        final Project project = findOneByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Project project = projectRepository.startProjectById(id);
        checkDateByStatus(project);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index) {
        final int size = projectRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        final Project project = projectRepository.startProjectByIndex(index);
        checkDateByStatus(project);
        return project;
    }

    @Override
    public Project startProjectByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = projectRepository.startProjectByName(name);
        checkDateByStatus(project);
        return project;
    }

    @Override
    public Project finishProjectById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Project project = projectRepository.finishProjectById(id);
        checkDateByStatus(project);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index) {
        final int size = projectRepository.size();
        if (!checkIndex(index,size)) throw new IndexIncorrectException();
        final Project project = projectRepository.finishProjectByIndex(index);
        checkDateByStatus(project);
        return project;
    }

    @Override
    public Project finishProjectByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Project project = projectRepository.finishProjectByName(name);
        checkDateByStatus(project);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final String statusChange) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (projectRepository.isAbsentById(id)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        System.out.println(statusChange);
        final Status status = Status.valueOf(statusChange);
        final Project project = findOneById(id);
        project.setStatus(status);
        checkDateByStatus(project);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String name, final String statusChange) {
        if (isEmpty(name)) throw new EmptyNameException();
        if (projectRepository.isAbsentByName(name)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        final Status status = Status.valueOf(statusChange);
        final Project project = findOneByName(name);
        project.setStatus(status);
        checkDateByStatus(project);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final int index, final String statusChange) {
        final int size = projectRepository.size();
        if (!checkIndex(index, size)) throw new IndexIncorrectException();
        if (projectRepository.isAbsentByIndex(index)) return null;
        Status[] statuses = Status.values();
        if (!checkInclude(statusChange,toStringArray(statuses))) throw new StatusIncorrectException(statusChange);
        final Status status = Status.valueOf(statusChange);
        final Project project = findOneByIndex(index);
        project.setStatus(status);
        checkDateByStatus(project);
        return project;
    }

}
