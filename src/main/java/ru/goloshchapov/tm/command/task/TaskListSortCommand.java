package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.enumerated.Sort;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListSortCommand extends AbstractTaskCommand{

    public static final String NAME = "task-sorted-list";

    public static final String DESCRIPTION = "Show task sorted list";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK SORTED LIST]");
        System.out.println("ENTER SORT TYPE");
        System.out.println(Arrays.toString(Sort.values()));
        System.out.println("DEFAULT TYPE = CREATED");
        final String sortType = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getTaskService().sortedBy(sortType);
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }
}
