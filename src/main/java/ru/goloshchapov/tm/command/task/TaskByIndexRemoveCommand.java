package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByIndexRemoveCommand extends AbstractTaskCommand{

    public static final String NAME = "task-remove-by-index";

    public static final String DESCRIPTION = "Remove task by index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = serviceLocator.getTaskService().removeOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }
}
