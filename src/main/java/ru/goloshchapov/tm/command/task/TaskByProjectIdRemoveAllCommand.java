package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectIdRemoveAllCommand extends AbstractTaskCommand{

    public static final String NAME = "task-remove-all-by-project-id";

    public static final String DESCRIPTION = "Remove all tasks by project id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE ALL TASKS FROM PROJECT]");
        System.out.println("ENTER PROJECT ID");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().removeAllByProjectId(id);
        if (tasks == null) throw new TaskNotFoundException();
        else tasks.clear();
    }
}
