package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskByProjectIdListCommand extends AbstractTaskCommand{

    public static final String NAME = "task-list-by-project-id";

    public static final String DESCRIPTION = "Show task list by project id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER PROJECT ID");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findAllByProjectId(id);
        if (tasks == null) throw new TaskNotFoundException();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }
}
