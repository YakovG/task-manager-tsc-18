package ru.goloshchapov.tm.command.auth;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.enumerated.Role;

public final class UsersByDefaultCreateCommand extends AbstractCommand {

    public static final String NAME = "user-create-by-default";

    public static final String DESCRIPTION = "Create default users";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getUserService().create("admin","admin", Role.ADMIN);
        serviceLocator.getUserService().create("test","test","test@test.tt");
    }

}
