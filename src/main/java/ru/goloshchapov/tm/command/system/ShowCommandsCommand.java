package ru.goloshchapov.tm.command.system;

import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ShowCommandsCommand extends AbstractCommand {

    public static final String ARGUMENT = "-cmd";

    public static final String NAME = "commands";;

    public static final String DESCRIPTION = "Show program commands";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final Collection<String> commands = serviceLocator.getCommandService().getListCommandNames();
        for (final String cmd:commands) System.out.println(cmd);
    }
}
