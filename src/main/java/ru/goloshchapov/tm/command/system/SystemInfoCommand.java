package ru.goloshchapov.tm.command.system;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.util.NumberUtil;

public final class SystemInfoCommand extends AbstractCommand {

    public static final String ARGUMENT = "-i";

    public static final String NAME = "info";

    public static final String DESCRIPTION = "Show system info";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.format(freeMemory);
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = NumberUtil.format(maxMemory);
        final String maxMemoryValue = isMaxMemory ? "no limit" : maxMemoryFormat;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.format(totalMemory);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.format(usedMemory);
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        System.out.println("Maximum memory (bytes): " + maxMemoryValue);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);
        System.out.println("Memory used by JVM (bytes): " + usedMemoryFormat);
    }
}
