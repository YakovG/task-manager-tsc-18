package ru.goloshchapov.tm.command.system;

import ru.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

import static ru.goloshchapov.tm.util.ValidationUtil.isEmpty;

public final class HelpCommand extends AbstractCommand {

    public static final String ARGUMENT = "-h";

    public static final String NAME = "help";

    public static final String DESCRIPTION = "Show terminal commands";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command: commands) {
            final String name = command.name();
            final String arg = command.arg();
            final String description = command.description();
            String result = "";
            if (!isEmpty(name)) result += name;
            if (!isEmpty(arg)) result += " [" + arg + "] ";
            if (!isEmpty(description)) result += " - " + description;
            System.out.println(result);
        }
    }
}
