package ru.goloshchapov.tm.command.system;

import ru.goloshchapov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    public static final String ARGUMENT = "-a";

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Show developer info";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Yakov Goloshchapov");
        System.out.println("E-MAIL: ygoloshchapov@tsconsulting.com");
    }

}
