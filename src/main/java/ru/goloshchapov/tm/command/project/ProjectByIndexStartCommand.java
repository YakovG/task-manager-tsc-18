package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByIndexStartCommand extends AbstractProjectCommand{

    public static final String NAME = "project-start-by-index";

    public static final String DESCRIPTION ="Start project by index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        final Project project = serviceLocator.getProjectService().startProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }
}
