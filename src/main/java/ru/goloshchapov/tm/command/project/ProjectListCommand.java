package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.model.Project;

import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand{

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Show project list";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = serviceLocator.getProjectService().findAll();
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }
}
