package ru.goloshchapov.tm.exception.incorrect;

import ru.goloshchapov.tm.exception.AbstractException;

public class StatusIncorrectException extends AbstractException {

    public StatusIncorrectException(final String message) {
        super("Error! Status " + message + " is incorrect...");
    }

}
