package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.enumerated.Role;
import ru.goloshchapov.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User create(String login, String password, String email, String role);

    User findUserById(String id);

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User removeUser(User user);

    User removeUserById(String id);

    User removeUserByLogin(String login);

    User setPassword(String userId, String password);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );
}
