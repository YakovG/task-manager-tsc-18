package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAllStarted(Comparator<Project> comparator);

    List<Project> findAllCompleted(Comparator<Project> comparator);

    Project add(String name, String description);

    void add(Project project);

    void remove(Project project);

    void clear();

    List<Project> sortedBy(String sortCheck);

    Project findOneById(String id);

    Project removeOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateOneById(String id, String name, String description);

    Project updateOneByIndex(Integer index, String name, String description);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project finishProjectById(String id);

    Project finishProjectByIndex(Integer index);

    Project finishProjectByName(String name);

    Project changeProjectStatusById(String id, String statusChange);

    Project changeProjectStatusByName(String name, String statusChange);

    Project changeProjectStatusByIndex(int index, String statusChange);

}
