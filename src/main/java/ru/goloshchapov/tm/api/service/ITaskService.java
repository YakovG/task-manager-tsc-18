package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllStarted(Comparator<Task> comparator);

    List<Task> findAllCompleted(Comparator<Task> comparator);

    Task add (String name, String description);

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> sortedBy(String sortCheck);

    Task findOneById(String id);

    Task removeOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task finishTaskById(String id);

    Task finishTaskByIndex(Integer index);

    Task finishTaskByName(String name);

    Task changeTaskStatusById(String id, String statusChange);

    Task changeTaskStatusByName(String name, String statusChange);

    Task changeTaskStatusByIndex(int index, String statusChange);

}
