package ru.goloshchapov.tm.api.repository;

import ru.goloshchapov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findUserById(String id);

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User removeUser(User user);

    User removeUserById(String id);


    User removeUserByLogin(String login);
}
